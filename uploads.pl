#!/usr/bin/perl
#+----------------------------------------------------------------------------+
#| Uploads Chat Room - for postage of Maglinks on ODCH                        |
#+-----------------------------------------------+----------------------------+
#| Author:  Derived from BigG's ChatBot by Anubis|
#| Created: 26/03/2006                           |
#+-----------------------------------------------+

use warnings;
use POSIX qw/strftime/;

# The name of the bot as it appears in the userlist
$botName = "Uploads";
# The version that the bot is
$version = 0.26;
# The path to the script - note: no trailing '/'
$scriptPath = "/home/hub/.opendchub/scripts";

# Command placeholder - what identifies a command
$cp = '-';
$c  = "";

# True and false
$true = 1;
$false = 0;

# The users that are currently connected to the chat
%users = ();

# The topic for the channel
@topic = ();

# the last 100 lines
@history = ();
$historyLine = 0;
$historyLength = 300;

$lengthOfPast = 400;
$lengthOfPastGrep = 30000;
$max_lines_returnable = 200;
#@winrars = 
#("Sjcs","Tachikoma","fastdude7","watzisneim","SemiPureAngel","Jay","Loki","Aenimal","Ace","Industrious_Thoughts","dloadergirl","Anubis","Smile","Murfle","Presario","Stoned_Turtle");

sub main() {
	&odch::register_script_name($botName);
	&odch::data_to_all("<$botName> $botName version $version active and auto-adding new users|");
	&load_data();
#-- Escape the command placeholder if relevant
	$c = escape_string($cp);
#	odch::data_to_all("$c|");
#	&odch::data_to_all("<$botName> $botName version $version loaded|");
	for ($i = 0; $i < $historyLength; $i++){
	      $history[$i] = "no history";

	}
}

# Triggers when any data passes through the hub
sub data_arrival() {
	my($user, $data) = @_;

#-- Check that the data is a pm to the bot
	if($data =~ /^\$To:\s\Q$botName\E\sFrom:\s\Q$user\E\s\$\<\Q$user\E\>\s(.*)\|/s) {
		$chatString = $1;
		
#------ Only command that can be used whilst not connected
		if($chatString =~ /^(?:$c)join$/i) {
				&add_user_connection($user);
		}
#------ Make sure that the user is connected
		elsif(!&is_disconnected($user)) {
#---------- Send the line that the user has said to everybody else...
			if($chatString =~ /^(?:$c)leave$/i || $chatString =~ /^(?:$c)quit$/i) {
					&remove_user_connection($user);
			}	
			elsif($chatString =~ /^(?:$c)rules$/i) {
				my $tmp = "";

				open( RULES, $scriptPath . "/rules" ) or &odch::data_to_all( "<$botName> Cannot open rules!|" );

				while( <RULES> ) {
					chomp;
					$tmp .= "$.. $_\n";
				}

				close RULES;

				odch::data_to_user( $user, "\$To: $user From: $botName \$<$botName> The rules have been set along the ages to be as follows:\n\n$tmp|" );
			}
			elsif($chatString =~ /^(?:$c)topic(\s+(.*)|)$/i) {
				if($1 eq "") {
					&send_topic($user);
				}
				elsif (&is_op($user)) {
					&set_topic($user, $2);
					&send_data( $botName, "<$botName> $user has changed the topic to: $2");
				}
			}
			elsif($chatString =~ /^(?:$c)commands$/i) {
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Commands:\n" .
$cp . "quit   - Exits the chat. To join again you will have to type .join\n" .
$cp . "leave  - Alias of $cp"."quit\n" .
$cp . "names  - Sends a list of all of the people who are currently connected to\n " .
      "          $botName.\n" .
$cp . "rules  - Gets the DT rules" );
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Operator only commands:\n" .
$cp . "invite <foo> - Invites the specified user to join the chat.  $botName will PM\n" .
      "                the user with an invite which they will have to respond with\n" .
	  "                the $cp"."join command\n" .
$cp . "kill <foo>   - Forcefully removes the specified user from the chat.  Can\n" .
      "                only be invoked on non-operators.\n" .
$cp . "ban <foo>    - Invokes ban.sh.  If foo is a nick, it will grab the IP from the hub\n" .
      "                [so do not use with stunnel users!!] otherwise, you may specify an\n" .
	  "                IPv4 Address xxx.xxx.xxx.xxx\n" .
$cp . "ip(s) <foo>  - Gets the ip address of the specified user/all users"
);
				}
			elsif($chatString =~ /^I\'m away\. State your business and I might answer later if you\'re lucky\.$/i) {
                                }
			elsif($chatString =~ /^(?:$c)history\s*(.*)/ || $chatString =~ /^(?:$c)h\s*(.*)/ )  {
				$hlength = 10;
				@chatStrA = split(/ /, $chatString);
				
				if( $chatStrA[1]) {
					if ($chatStrA[1] =~ /^\d+$/){
						$hlength = $chatStrA[1];
					}
				}
				if ($hlength > $historyLength) {
					$hlength = $historyLength;
				}
				$temp = "";
				for ($i = 0; $i < $hlength; $i++) {
				    $temp .= "\n $history[($historyLine - $hlength + $i)%$historyLength]";
				}
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
						"[Users: $botName]$temp|");
            }
            elsif($chatString =~ /^(?:$c)past\s(.*)$/ || $chatString =~ /^(?:$c)p(\s(.*))?$/) {
				@chatStrA = split(/ /, $chatString);
					if ($chatStrA[1] =~ /^\d+$/){
						$hours = $chatStrA[1];
					} else {
						$hours = 24;
					}
					$temp = getUploads($hours);
					&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
						"[Users: $botName] the past!!!! $temp|");
			}
		 elsif($chatString =~ /^(?:$c)pastgrep\s(\d\d*)\s(..*)$/ || $chatString =~ /^(?:$c)pg\s(\d\d*)\s(..*)$/) {
                                @chatStrA = split(/ /, $chatString);
                                        my $grep_result = grepUploads($1, lc($2));
                                        &odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
                                                "[Users: $botName] the past grep!!!! $grep_result|");
                        }
			elsif (odch::get_type($user) >= 8 || $chatString =~ /magnet/) {
				&send_data( $user, "<$user> $chatString" );
				$history[$historyLine] = "<$user> $chatString";
				$historyLine = $historyLine + 1;
				if ($historyLine == $historyLength) {
					$historyLine = 0;
				}
				&log_uploads("<$user> $chatString");
			} else {
				odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
					"Mag links only in Uploads, U must be registered to post decriptions in here \n If u are having trouble go to main chat (that is the tab with chaotic neutral in it or 127.0.0.1 in it.|");
			}
		}
#------ User is not connected to the chat
		else {
			&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
					"You are not connected to the chat, please type $cp"."join.|");
		}
	}
}




# Sends the data to everyone connected to the chatbot, if they are online
sub send_data() {
	my($user, $data) = @_;
	my(@sdnicks) = split(/\s/, odch::get_user_list());
	foreach $nick (@sdnicks) {
		if($user ne $nick && odch::get_type($nick) != 0 && !&is_disconnected($user)) {
			odch::data_to_user($nick, "\$To: $nick From: $botName \$$data|");
		}
	}
}

sub escape_string() {
	my($string) = @_;
	$string =~ s/$_/$_/ for qw(\\\ \| \\( \\) \[ \{ \$ \+ \? \. \* \/ \^);
	return $string;
}

sub remove_user_connection() {
	my($user) = @_;
	if (odch::get_type($user) >= 8) {
		$users{$user} = $true;
		&save_data();
	}
}

sub add_user_connection() {
	my($user) = @_;
	if (&is_disconnected($user)) {
		delete $users{$user};
		&save_data();
	}
	&connect_message($user);
}

sub connect_message() {
	my($user) = @_;

	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Welcome to $botName! ".
			"Type $cp"."commands for available commands|");
	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> --- Topic for $botName is: $topic[3]|");
	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> --- Topic set by $topic[0] [$topic[1]] " .
			gmtime($topic[2]) . "|");
}

sub op_admin_connected(){
	my ($user) = @_;
	&connect_message($user);
}
sub op_connected(){
	my ($user) = @_;
	&connect_message($user);
}
sub reg_user_connected(){
	my ($user) = @_;
	&connect_message($user);
}
sub new_user_connected(){
	my ($user) = @_;
	&connect_message($user);
}
# Topic shiz
sub send_topic() {
	my(@user) = @_;
	&send_data($user, "<$botName> --- Topic for $botName is: $topic[3]");
	&send_data($user, "<$botName> --- Topic set by $topic[0] [$topic[1]] " . gmtime($topic[2]) ."");
}

sub set_topic() {
	my($user, $data) = @_;
	my($hostname) = odch::get_hostname($user);
	my($time) = time;
	@topic = ($user, $hostname, $time, $data);
	&send_data($botName, "<$botName> --- Topic set to \"[$topic[3]]\" by $topic[0]");
	&save_data();
}

# Returns true if the user is an operator
sub is_op() {
	my($user) = @_;
	if(odch::get_type($user) >= 4) {
		return $true;
	}
	else {
		return $false;
	}
}


# Returns true if the user is disconnected to the chatbot all user by default are connected
sub is_disconnected() {
	my($user) = @_;
	if($users{$user} eq $true) {
		return $true;
	}
	else {
		return $false;
	}
}


# File loading - config, who is connected etc
sub load_data() {
	open(DATA, "$scriptPath/$botName"."data"); # or odch::data_to_all("<$botName> Unable to load $botName data!|");
	
	while(<DATA>) {
		if($_ =~ /^\[topic\]\s(.*)\s(.*)\s(\d+)\s(.*)\n/) {
			@topic = ($1, $2, $3, $4);
		}
		elsif($_ =~ /^\[users\]\s(.*)\n/) {
			foreach $user (split(/\s/, $1)) {
				$users{$user} = 1 if $user ne "";
			}
		}
	}
	close(DATA);
}

# Save the data to file
sub save_data() {
	my($data) = "";
	
#-- Add topic to data string
	$data .= "[topic] @topic\n[users] ";

#-- Add the current users
	foreach $user (keys %users) {
		$data .= " $user";
	}
	
	open(DATA, ">$scriptPath/$botName"."data") or odch::data_to_all("<$botName> Unable to load $botName data!|");
	print DATA "$data\n";
	close(DATA);
}
sub log_uploads() {
	my($chatString) = @_;
		$fileName = int(time() / 60 / 60);
		open (UPLS, ">>$scriptPath/../uploads/$fileName") or odch::data_to_all("<$botName> could not write to uploads data|");
		print UPLS "$chatString \n";
		close(UPLS);
}

# Give it a number of hours and will return all uploads for that many hourse back
sub getUploads() {
	my($hours) = @_;
	if ($hours > $lengthOfPast) {
		$hours = $lengthOfPast;
	}
	$endtime = int(time() / 60 / 60);
	$starttime = $endtime - ($hours - 1);
	$thePast = "This is the past $hours hours happy downloading\n";
	for ($fileName = $starttime; $fileName <= $endtime; $fileName++){
		$fileExists = 1;
		open (UPLS, "$scriptPath/../uploads/$fileName") or $fileExists = 0;
		if ($fileExists) {
			@contents = <UPLS>;
			foreach $pastLine (@contents) {
				$thePast .= $pastLine;
			}		
			close(UPLS);
		}
	}
	return  $thePast;
}

sub grepUploads() {
        my($hours, $grep) = @_;
        if ($hours > $lengthOfPastGrep) {
                $hours = $lengthOfPastGrep;
        }
        my $endtime = int(time() / 60 / 60);
        my $starttime = $endtime - ($hours - 1);
        my $thePast = "This is the past $hours hours containing $grep happy downloading\n";
	my $lines_returned = 0;
        for ( $fileName = $starttime; $fileName <= $endtime; $fileName++){
                 $fileExists = 1;
                open (UPLS, "$scriptPath/../uploads/$fileName") or $fileExists = 0;
                if ($fileExists) {
                        @contents = <UPLS>;
                        foreach $pastLine (@contents) {
				if (lc($pastLine) =~ /$grep/) {
	                                $thePast .= $pastLine;
					$lines_returned++;
				}
				if ( $lines_returned >= $max_lines_returnable) {
					my $next_hours = $endtime - $fileName - 1;
					return $thePast . "\n too many lines to get the rest try " . $next_hours . "hours";
				}
                        }
                        close(UPLS);
                }
        }
        return  $thePast . "\n lines returned: ". $lines_returned;
}

#------------------------------------------------------------------------------

# EOF
