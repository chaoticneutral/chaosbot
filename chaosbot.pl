#!/usr/bin/perl

#----------------------------------------------------------------#
# Chaotic Neutral Hub Bot - ChaosBot
# Created by KY of Chaotic Neutral
# Far too much time was sunk into this bot >_>
#----------------------------------------------------------------#

#-------------------------- Include all the additional CPAN files
use Config::IniFiles;			#For the conf file
use Switch;				#For Switch/case statements
use Text::Tabs;				#For expand in sendmessage
use IPC::System::Simple qw(capture);	#For the capture() command when calling external scripts
use DBI;				#for connecting to the logging database;
use DateTime;				#for timezone support when logging chat

#-------------------------- Set the location of the configfile
my $ConfigFile = "/home/hub/.opendchub/scripts/chaosbot.conf";
tie my %ini, 'Config::IniFiles', (-file => $ConfigFile);
my %Config = %{$ini{"config"}};

$botname = $Config{botname};
$topic = $Config{topic};
$access = $Config{allow_external};

#-------------------------- Set up log system if it's not already in place
mkdir $Config{dbloc}, 0755 unless -d $Config{dbloc};
my $dbh = DBI->connect("dbi:SQLite:" . $Config{dbloc} . $Config{db},"","") or error("dbconnect", "Unable to connect to the db");
my $logname = $Config{log}; 
my $infolog = $Config{infolog};
my $dailystatus = $Config{statusdaily};
my $weeklystatus = $Config{statusweekly};
my $longstatus = $Config{statuslong};

#-------------------------- Set up variables for the stats
our $connections = 0;
our $disconnections = 0;
our $searches = 0;
our $maxu = 0;
our $minu = 0;
our $maxs = 0;
our $mins = 0;
my @days = qw(mon tues weds thurs fri sat sun);

#--------------------------- Set up variables for alert messages
our $alertmsg = $Config{alertmsg};
our $alertiterate = $Config{alertiterate};

#--------------------------- Create the tables if they're not there and get default values if they are there.
my @tables = $dbh->tables;
unless (grep $_ =~ $logname, @tables) {
  $dbh->do("CREATE TABLE $logname (id INTEGER PRIMARY KEY AUTOINCREMENT, unixtime, timestamp, nick, chat)");
  $dbh->do("CREATE TABLE $infolog (id INTEGER PRIMARY KEY AUTOINCREMENT, nick, pjtime, pjshare, tjtime, tjshare, treg, tclient, tconnect, tap, disc, vip)");
  $dbh->do("CREATE TABLE $dailystatus (id INTEGER PRIMARY KEY AUTOINCREMENT, time, users, share, connections, disconnections, searches)"); 
  $dbh->do("CREATE TABLE $weeklystatus (day_id, maxs, mins, maxu, minu)");
  $dbh->do("CREATE TABLE $longstatus (id INTEGER PRIMARY KEY AUTOINCREMENT, scriptstart, totalmaxs, totalmins, totalmaxu, totalminu)");
  my $days_query = "INSERT INTO $weeklystatus (day_id, maxs, mins, maxu, minu) VALUES (?, ?, ?, ?, ?)";
  foreach (@days) {
    my $days_handle = $dbh->prepare($days_query);
    $days_handle->execute($_, 0, 0, 0, 0);
  }
  my $scriptstart_query = "INSERT INTO $longstatus (scriptstart) VALUES (?)";
  my $scriptstart_handle = $dbh->prepare($scriptstart_query);
  $scriptstart_handle->bind_param( 1, time());
  $scriptstart_handle->execute;
}else{
  my $clearonline = "UPDATE info SET disc=tjtime WHERE disc < tjtime";
  my $clearhandle = $dbh->prepare($clearonline);
  $clearhandle->execute;
  my $statquery = "SELECT connections, disconnections, searches FROM $dailystatus ORDER BY id DESC LIMIT 1";
  my $stathandle = $dbh->prepare($statquery);
  $stathandle->execute();
  $stathandle->bind_columns(\$connections, \$disconnections, \$searches);
  while ($stathandle->fetch()) {
  }
  my $lstatquery = "SELECT totalmaxs, totalmins, totalmaxu, totalminu FROM $longstatus";
  my $lstathandle = $dbh->prepare($lstatquery);
  $lstathandle->execute();
  $lstathandle->bind_columns(\$maxs, \$mins, \$maxu, \$minu);
  while ($lstathandle->fetch()) {
  }
  my $scriptstart_update_query = "UPDATE $longstatus SET scriptstart = ? WHERE id = 1";
  my $scriptstart_update_handle = $dbh->prepare($scriptstart_update_query);
  $scriptstart_update_handle->bind_param( 1, time());
  $scriptstart_update_handle->execute;
}

sub main() {
  sendmessage("","",4,"$botname version $Config{version} - loaded and ready!");
  $Config{c} = escape_string($Config{cp});
  odch::register_script_name($botname);
  sendmessage("","",1,"\$HubName $Config{topic}");
}

#---------------- sub data_arrival
# Whenever any data passes through the hub
# it gets passed to the script and runs
# through the data_arrival sub
#----------------
sub data_arrival() {
  my($user, $data) = @_;
  $data =~ s/[\r\n]+/ /g;
  if ($data =~ /^\$Search/){
    $searches++;
  }
  # Regular expression that matches all chat
  if( $data =~ /^\<\Q$user\E\>\s(.*)\|/) {
    my $chat = $1;
    # Log all chat coming into the hub in the db
    chatlog($user, $chat);
    # Call the external 'line' perl script.
    # This is called every single line so is
    # great for questions/answer or things like
    # tell.
    eval {
      $lineresult = capture($Config{perl}, $Config{scriptPath} . "line", $user, $chat);
    };
    if ($@) {
      error("line", $user, $@);
    }
    $k = 0;
    # Iterate through responses from line and 
    # deliver messages to the appropriate channels
    my ($lineuser, $lineother, $linetype, $linemsg) = ('','','','');
    foreach (split(/¡!KY1!¡/, $lineresult)) {
      switch ($k) {
        case 0 { $lineuser = $_; }
        case 1 { $lineother = $_; }
        case 2 { $linetype = $_; }
        case 3 { $linemsg = $_; }
        case 4 { 
          $lineodch = $_;
          if ($lineodch ne 'null') {
            odch($lineodch, $lineuser, $lineother, $lineother);
          }else{
            sendmessage($lineuser,$lineother,$linetype,$linemsg);
          }
          $k = -1;
         }
      }
      $k++;
    }
    # Pattern match whatever commands are. Default usecase
    # has this matching -command as - is the default command
    # key. Bot must be reloaded for change to command key to
    # take effect (or $Config reloaded. TODO)
    if ($chat =~ /^(?:$Config{c})(\w+)\s?(\w*)\s?(.*)/) {
      my $command = $1;
      my $args1 = $2;
      my $args2 = $3;
      my $userlevel = perm($user);
      my $userip = odch::get_ip($user);
      my $hubdata = odch::get_variable("total_share") . ':' . odch::get_variable("hub_uptime");
      %output = ();
      # Capure the result of commands executed, potentially think about
      # registering available commands in the db and as a static array
      # to avoid waste of perl interpreter. 
      # IDEA: -addcommand adds the command name to a database table of commands
      # and also adds it to an assoc array of names in the bot. 
      # When someone uses a command it checks to see if the command is a key and if 
      # it is enabled. That way it won't have to try running commands that aren't in use.
      # Can run a -commandslist as op to see all available commands (scan the directory)
      # TODO
      eval {
        $result = capture($Config{perl}, $Config{commandPath} . $command, $user, $userlevel, $args1, $args2, $userip, $hubdata);
      };
      if ($@) {
        error("command", $user, $@);
      }else{
        $i = 0;
        foreach(split(/Þ/, $result)) { #� is the splitting character. This makes the return array.
          switch ($i) {
            case 0 { $output{user} = $_; }
            case 1 { $output{touser} = $_; }
            case 2 { $output{type} = $_; }
            case 3 { $output{message} = $_; }
            case 4 { $output{odch} = $_; }
            case 5 { 
              $output{fire} = $_;

              # Allows command to send multiple messages.
              if ($output{odch} ne 'null') {
                odch($output{odch}, $output{user}, $output{touser}, $output{message});
              }
              elsif ($output{fire} == 1 || perm($user) >= 2) {
                sendmessage($output{user},$output{touser},$output{type},$output{message});
              }
              $i = -1;
            }
          }
          $i++;
        }
      }
    }
    # Say, enables users to say things as other users (if they are admin)
    elsif ($chat =~ /^!say\s(\w+)\s(.*)/ && perm($user) > 2) {
      sendmessage($1,"",10,$2);
      sendmessage("","",7,"$user just used say: <$1> $2");
      chatlog($1, $2);
    }
  }
}

#---------------- sub escape_string
## Escapes the command key primarily but
## can be used to escape anything.
sub escape_string() {
  my($string) = @_;
  $string =~ s/$_/$_/ for qw(\\\ \| \\( \\) \[ \{ \$ \+ \? \. \* \/ \^);
  return $string;
}
#---------------- sub op_admin_connected
## Fires when an admin connects
sub op_admin_connected() {
  my ($user) = @_;
  login($user);
}

#---------------- sub op_connected
## Fires when an op connects
sub op_connected() {
  my ($user) = @_;
  login($user);
}
sub reg_user_connected() {
  my ($user) = @_;
  login($user);
}
sub new_user_connected() {
  my ($user) = @_;
  #if (namecheck($user) == 1) {
    login($user);
  #}
}

#---------------- sub login
# Runs all of the processes that complete
# whenever any user connects to the hub.
# This runs for all users as it is called
# by each individual connection sub
#----------------
sub login() {
  if (odch::count_users() > $maxu) {
    $maxu = odch::count_users();
  }
  if (odch::get_variable(total_share) > $maxs) {
    $maxs = odch::get_variable(total_share);
  } 
  $connections++;
  my ($login_user) = @_;
  my $login_user_perm = perm($login_user);
  my $login_user_ip = get_ip($login_user);
  my $login_user_share = get_share($login_user);
  my $login_user_actpass = actpass($login_user);
  %login_output = ();
  eval {
    $login_result = capture($Config{perl}, $Config{scriptPath} . $Config{login}, $login_user, $login_user_perm, $login_user_ip, $login_user_share, $login_user_actpass);
  };
  if ($@) {
    error("login", $login_user, $@);
  }
  $j = 0;
  foreach(split(/¡!KY1!¡/, $login_result)) { #� is the splitting character. This makes the return array.
    switch ($j) {
      case 0 { $login_output{login} = $_; }
      case 1 { $login_output{pm} = $_; }
      case 2 { $login_output{private} = $_; }
      case 3 { $login_output{public} = $_; }
    }
  $j++;
  }
  my $login_query = "SELECT count(*) FROM $infolog WHERE UPPER(nick) = UPPER(?)";
  my $login_handle = $dbh->prepare($login_query);
  $login_handle->execute($login_user);
  if ($login_handle->fetch()->[0]) {
    $login_user_new = 0;
  }else{
    $login_user_new = 1;
  }
  sendmessage($login_user,"",3,$login_output{pm});
  sendmessage($login_user,"",2,$login_output{private});
  sendmessage("","",4,$login_output{public});
  if ($login_output{login} == 0) {
    odch::kick_user($login_user);
  }else{

    $login_spaces = '---------------------------------------------------------------------------';
    $login_string = "***===[" . $login_user . "::" . $login_user_actpass . "::" . intext($login_user) . "::" . perm_name($login_user) . "]===***"; 

    sendmessage($login_user,"",0,"\$HubName $topic");
    if ($login_user_new == 0) {
      sendmessage($login_user,"",2,"\n$login_spaces\n\n\t\t\tWelcome back to $Config{hubname} $login_user\n\n\t\t\t$login_string\n\n\t\t\tTopic: $topic\n\n$login_spaces");
      my $login_temp_query = "UPDATE $infolog SET tjtime = ?, tjshare = ?, treg = ?, tclient = ?, tconnect = ?, tap = ? WHERE nick = ?";
      my $login_temp_handle = $dbh->prepare($login_temp_query);
      $login_temp_handle->bind_param( 1, time());
      $login_temp_handle->bind_param( 2, $login_user_share);
      $login_temp_handle->bind_param( 3, $login_user_perm);
      $login_temp_handle->bind_param( 4, client($login_user));
      $login_temp_handle->bind_param( 5, $login_user_ip);
      $login_temp_handle->bind_param( 6, $login_user_actpass);
      $login_temp_handle->bind_param( 7, $login_user);
      $login_temp_handle->execute();
    }else{
      sendmessage($login_user,"",2,"\n$login_spaces\n\n\t\t\tWelcome to $Config{hubname} for the first time $login_user\n\n\t\t\t$login_string\n\n\t\t\tTopic: $topic\n\n$login_spaces");
      my $login_perm_query = "INSERT INTO $infolog (nick, pjtime, pjshare, tjtime, tjshare, treg, tclient, tconnect, tap, disc, vip) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      my $login_perm_handle = $dbh->prepare($login_perm_query);
      $login_perm_handle->bind_param( 1, $login_user);
      $login_perm_handle->bind_param( 2, time());
      $login_perm_handle->bind_param( 3, $login_user_share);
      $login_perm_handle->bind_param( 4, time());
      $login_perm_handle->bind_param( 5, $login_user_share);
      $login_perm_handle->bind_param( 6, $login_user_perm);
      $login_perm_handle->bind_param( 7, client($login_user));
      $login_perm_handle->bind_param( 8, $login_user_ip);
      $login_perm_handle->bind_param( 9, $login_user_actpass);
      $login_perm_handle->bind_param( 10, time());
      $login_perm_handle->bind_param( 11, 0);
      $login_perm_handle->execute();
    }
  }
}

#---------------- sub user_disconnected
# Ru15 ) . "minutes");ns all the disconnection processes
# for the user who departs
#----------------
sub user_disconnected() {
  $disconnections++;
  if (odch::count_users() < $minu) {
    $minu = odch::count_users();
  }
  if (odch::get_variable(total_share) < $mins) {
    $mins = odch::get_variable(total_share);
  }
  my ($user_disconnected_user) = @_;
  my $user_disconnected_query = "UPDATE $infolog SET disc = ? WHERE nick = ?";
  my $user_disconnected_handle = $dbh->prepare($user_disconnected_query);
  $user_disconnected_handle->bind_param( 1, time());
  $user_disconnected_handle->bind_param( 2, $user_disconnected_user);
  $user_disconnected_handle->execute();
}

sub info_log() {
#log the status, connects, disconnects
}

#---------------- sub get_ip
# Returns the IP of the user
#---------------- 
sub intext() {
  my ($intext_user) = @_;
  if (get_ip($intext_user) == '127.0.0.1') {
    return 'Internal';
  }else{
    return 'External';
  }
}

sub get_ip() {
  my ($get_ip_user) = @_;
  return odch::get_ip($get_ip_user);
}

#---------------- sub get_share
# Returns the share of the user
# in long form (all bytes included)
#----------------
sub get_share() {
  my ($get_share_user) = @_;
  return odch::get_share($get_share_user);
}

#---------------- sub client
# Returns the name of the client
# that the user is connected with
# ApexDC++, RSX++ etc
#----------------
sub client() {
  my ($client_user) = @_;
  my $client_description = odch::get_description($client_user);
  $client_description =~ s/.*<(.*)\sV.*/\1/;
  return $client_description;
}

#---------------- sub actpass
# Returns either active or passive
# depending on the user's connection
# settings
#----------------
sub actpass() {
  my ($actpass_user) = @_;
  $actpass_description = odch::get_description($actpass_user);
  if ($actpass_description =~ /:A,/){
    $actpass = Active;
  }else{
    $actpass = Passive;
  }
  return $actpass;
}

#---------------- sub perm
# Will return a number depending on the 
# user that it processes
# 3-admin, 2-op, 1-reg, 0-anon
#
#---------------- sub perm_name
# Returns the word name that is 
# equivalent to the number
#
#----------------
sub perm_name() {
  my($perm_name_user) = @_;
  switch (perm($perm_name_user)) {
    case 0 { return $Config{unreg}; }
    case 1 { return $Config{reg}; }
    case 2 { return $Config{op}; }
    case 3 { return $Config{boss}; }
  }
}

sub perm() {
  my($perm_user) = @_;
  odch::check_if_registered($perm_user);
}

#---------------- sub odch
# Processes odch commands from 
# external modules
#----------------
sub odch() {
  my($odch_func, $user, $victim, $arg) = @_;
  switch($odch_func) {
    case "kick" { 
      sendmessage($victim,$user,3,$arg);
      sendmessage("","",4,"$victim is being kicked from $Config{hubname} by $user");
      odch::kick_user($victim); 
    } 
    case "tban" {
      @argarray = split(/\s+/, $arg);
      $nickbantime = $argarray[0];
      sendmessage($victim,$user,3,$arg);
      sendmessage("","",4,"$victim is being tbanned from $Config{hubname} by $user");
      odch::add_nickban_entry("$victim $nickbantime");
      odch::kick_user($victim);
    }
    case "rmban" {
      sendmessage("","",4,"$victim unbanned from $Config{hubname} by $user");
      odch::remove_nickban_entry($victim);
    }
    case "topic" { 
      $topic = $arg;
      sendmessage("","",4,"Topic set by $user to $topic");
      sendmessage("","",1,"\$HubName $topic"); 
    }
    case "illegal" {
      sendmessage("","",3,$arg);
      odch::kick_user($victim);
    }
    case "access" {
      switch ($victim) {
        case 0 { $access = 0; }
        case 1 { $access = 1; } 
      }
      sendmessage("","",4,$arg); 
    } 
    case "alert" {
      our $alertiterate = $victim;
      our $alertmsg = $arg;
      our $alertno = 0;
      sendmessage("","",4,"$alertmsg set to be displayed every " . ( $alertiterate * 15 ) . " minutes");
    }
  }		
}

sub hub_timer() {
  $alertno = (($alertno > 0) ? $alertno : 0);
  if ($alertmsg && ($alertiterate == $alertno)) {
    sendmessage("","",4,$alertmsg);
    $alertno = 0;
  }
  $alertno++;
  my $hub_timer_query = "INSERT INTO $dailystatus (time, users, share, connections, disconnections, searches) VALUES(?, ?, ?, ?, ?, ?)";
  my $hub_timer_handle = $dbh->prepare($hub_timer_query);
  $hub_timer_handle->bind_param( 1, time());
  $hub_timer_handle->bind_param( 2, odch::count_users());
  $hub_timer_handle->bind_param( 3, odch::get_variable(total_share));
  $hub_timer_handle->bind_param( 4, $connections);
  $hub_timer_handle->bind_param( 5, $disconnections);
  $hub_timer_handle->bind_param( 6, $searches);
  $hub_timer_handle->execute;
  my $hub_timer_query2 = "UPDATE $longstatus SET totalmaxs = ?, totalmins = ?, totalmaxu = ?, totalminu = ? WHERE id = 1";
  my $hub_timer_handle2 = $dbh->prepare($hub_timer_query2);
  $hub_timer_handle2->bind_param( 1, $maxs);
  $hub_timer_handle2->bind_param( 2, $mins);
  $hub_timer_handle2->bind_param( 3, $maxu);
  $hub_timer_handle2->bind_param( 4, $minu);
  $hub_timer_handle2->execute;
  print("\n" . time() . " Logging Data Now\n");
#Add in something here
##that checks the day,
##then does a mysql insert with a if statement
## "update table set field=$total WHERE id='$id' AND field<$total"
}

sub error() {
  my ($sub, $user, $msg) = @_;
  $errorresult = capture($Config{perl}, $Config{scriptPath} . "error", $sub, $user, $msg);
  $l = 0;
  my ($erroruser, $errorother, $errortype, $errormsg) = ('','','','');
  foreach (split(/¡!KY1!¡/, $errorresult)) {
    switch ($l) {
      case 0 { $erroruser = $_; }
      case 1 { $errorother = $_; }
      case 2 { $errortype = $_; }
      case 3 { $errormsg = $_;
          $errorodch = $_;
          sendmessage($erroruser,$errorother,$errortype,$errormsg);
          $l = -1;
      }
    }
  $l++;
  }
}

#user log in subs
##put in all stuff to record searches/disc/conn etc to config also
## need user connect subs
##tban put into odch
##need a log error sub for errors in commands
##maybe with a debug variable in the config
##need a chat logging sub
##need need hub topic support, not sure how can do
##need to make a sub for odch commands
##pass perm of victim to new script
##odch::data_to_all("<herp> derp derp|");
## odch::gag_user($user, 1); - gag chat
##* odch::gag_user($user, 2); - gag download
##* odch::gag_user($user, 3); - gag search
##* odch::ungag($user); - ungag all
#
#sendmessage($user,$touser,$type,$message);
##For example when sending a message that everyone will see in Main Chat
##sendmessage($user,"",4,"OK $user I'll pass on the message to $target when I next see them!");
##0 raw data to user
##1 Message everybody sees in main chat from the hub (Similar to the MOTD)
##2 Message from the bot in main chat that only the recipient sees
##3 PM from the bot to a user in a seperate PM window
##4 Message everybody sees in main chat from the bot
##5 PM from the bot to all users. (Mass message)
##6 Spoofs PM from user->touser
##7 Messages all OPs
##8 PM from the hub to a user in a seperate PM window
##9 PM from one user to another that will only show to the to user
##10 Spoofs chat in mainchat
sub sendmessage() {
  my ($user, $touser, $type, $message) = @_;
  $message =~ s/\r?\n/\r\n/g;
  $message =~ s/\|//g;
  $message = expand($message);  # mmm Tabs...
  if ($message) {
    switch($type) {
      case 0 { odch::data_to_user($user, $message."|"); }
      case 1 { odch::data_to_all($message."|"); }
      case 2 { odch::data_to_user($user, "<$botname> $message|"); }
      case 3 { odch::data_to_user($user, "\$To: $user From: $botname \$<$botname> $message|"); }
      case 4 { 
               odch::data_to_all("<$botname> $message|");
               chatlog($botname, $message);
             }
      case 5 { odch::data_to_all("\$To: $user From: $botname \$<$botname> $message|"); }
      case 6 {
               odch::data_to_user($user,"\$To: $user From: $touser \$$message|");
               odch::data_to_user($touser,"\$To: $touser From: $user \$$message|");
             }
      case 7 { &sendtops("", "$message|"); }
      case 8 { odch::data_to_user($user,"\$To: $user From: $botname \$$message|"); }
      case 9 { odch::data_to_user($touser,"\$To: $touser From: $user \$$message|"); }
      case 10 { odch::data_to_all("<$user> $message|"); }
    }
  }
}

sub sendtops() {
  my($user, $message) = @_;
  foreach (split(/\s+/, odch::get_user_list())) {
    if (odch::get_type($_) >= 16) {
      odch::data_to_user($_, "\$To: $_ From: $botname \$<$botname> $message|");
    }
  }
}

sub chatlog() {
  my ($user, $chat) = @_;
  unless ($chat =~ /^!.*/) {
    # Insert into DB
    my $query = "INSERT INTO $logname (unixtime, timestamp, nick, chat) VALUES (?, ?, ?, ?)";
    my $handle = $dbh->prepare($query);
    $handle->bind_param( 1, time());
    $handle->bind_param( 2, timestamp());
    $handle->bind_param( 3, $user);
    $handle->bind_param( 4, $chat);
    $handle->execute;
    # Insert into flag file for live chat
    open( FLAG, '>' . $Config{scriptPath} . 'flagfile');
    print FLAG '<' . $user . '> ' . $chat;
    close (FLAG);
  }
}

sub timestamp() {
  my $now_string = DateTime->now();
  $now_string->set_time_zone($Config{timezone});
  return $now_string->strftime("%Y-%m-%d %H:%M:%S");
}

