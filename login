#!/usr/bin/perl -w

## Put extra dependencies here.
use DBI;

## Do not alter after this point until stated ##

## UNIQUE LOG IN SCRIPT

# $login_result = capture($Config{perl}, $Config{scriptPath} . $config{login}, $login_user, $login_user_perm, $login_user_ip, $login_user_share, $login_user_actpass);

use strict;
use warnings;
use Config::IniFiles;

my $ConfigFile = "/home/hub/.opendchub/scripts/chaosbot.conf";
tie my %ini, 'Config::IniFiles', (-file => $ConfigFile);
my %Config = %{$ini{"config"}};

my $x = '';
my @array = ();
my $user = $ARGV[0];
my $user_perm = $ARGV[1];
my $user_ip = $ARGV[2];
my $user_share = $ARGV[3];
my $user_actpass = $ARGV[4];
$0 =~ s/^.*?(\w+)[\.\w+]*$/$1/;

sub output( @ ) {
  my @array = @_;
  for($x=0;$x<@array;$x++) {
    $array[$x] = $array[$x] . "¡!KY1!¡";
  }

  print @array;
}
## Do not alter before this point ##

## Info Block ##

#1# Command Name: login
#2# Description: This is the login script.
#3# Command used by: Users
#4# Author: KY
#5# Date Created: 27/11/2011
#6# Date added to Bot: 27/11/2011

## Variables available to the script:

# User initiating the script :: $user
# Permission level of the User :: $user_perm
#	0 = Non registered
#	1 = Registered
#	2 = Op
#	3 = Admin
# args1 (The first word after the command) :: $arg_first
# args2 (The second word after the command) :: $arg_second
# eg -testcommand args1 args2

## Output values

# Fill in the required return values here and leave output(@array); as is.
# @array[0] Is the user initiating the script
# @array[1] Is the user the script acts upon
# @array[2] Is the type of message to be sent using the sendmessage function
# @array[3] Is the message to be sent back
# @array[4] If there is an odch function to be used name it here. If not leave blank or null
# @array[5] $fire determines whether or not the script actually fires.

## Message Types (for @array[2])

# The most oft used 'types' when sending to sendmessage are as follows:
# 2 Displays in main chat ONLY to the user
# 3 Sends a PM to the user
# 4 Sends to main chat for everyone

## End Info Block ##

## Place your module specific code here.

my $login = 0;
my $pm = '';
my $private = '';
my $public = '';
my $tell = '';

sub namecheck( @ ) {
  $login = 0;
  my ($namecheck_user) = @_;
  if ($user_ip !~ /127.0.0.1/ && $Config{allow_external} == 0){
    $pm .= "Unauthorised access. Please contact a hub operator $Config{webcontact}\n";
  }
  elsif ($namecheck_user =~ /unconfigured-valknut-client/i) {
    $pm .= "$user, Please configure your username as per the instructions here: http://chaoticneutral.ath.cx/faq/mac-name.html\n";
  }
  elsif ($namecheck_user =~ /.*\W.*/ || $namecheck_user =~ /.*\W.*/) {
    $pm .= "Your name is $user, Please remove all disallowed characters from your username\n\nAllowed characters:\n * Letters\n * Numbers\n * Underscores\n";
  }
  elsif ($namecheck_user =~ /.*\/.*/ || $namecheck_user =~ /.*\\.*/) {
    $pm .= "Please remove all slashes from your username.\n";
  }
  elsif (length($namecheck_user) < $Config{min_username}) {
    $pm .= "Please ensure your name is longer than " . $Config{min_username} . " characters then attempt to log on again.";
  }
  else{
    $login = 1;
  }
  return $login;
}

if ($user_perm == 0 && namecheck($user) == 1) {
  $login = 1;
}
if ($user_perm >= 1) {
  $login = 1;
}
if ($user_perm > 1 && $Config{opgreetingon}) {
  $public = $Config{opgreeting} . " " . $user;
}
if ($user_perm <= 1 && $Config{usergreetingon}) {
  $public = $Config{usergreeting} . " " . $user;
}
if ($user_perm <= 1 && $user_share <= 1) {
  $login = 0;
  $pm .= "You are not sharing enough. Please share more than $user_share bytes.\n";
}

if ($login == 1) {
  my $dbh = DBI->connect("dbi:SQLite:" . $Config{dbloc} . $Config{db},"","");
  my $telldb = 'tell';
  my @tables = $dbh->tables;
  unless (grep $_ =~ $telldb, @tables) {
    $dbh->do("CREATE TABLE $telldb (id INTEGER PRIMARY KEY AUTOINCREMENT, unixtime, timestamp, fromnick, tonick, message)");
  }

  my $query = "SELECT timestamp, fromnick, message FROM tell WHERE UPPER(tonick)=UPPER(?)";
  my $handle = $dbh->prepare($query);
  $handle->execute($user);
  my ($timestamp, $fromnick, $message) = ('', '', '');
  $handle->bind_columns(\$timestamp, \$fromnick, \$message);
  while ($handle->fetch()) {
    $tell .= "Hi $user, at [" . $timestamp . "], " . $fromnick . " told me to tell you: " . $message . "\n";
  }
  if ($tell) {
    $pm .= $tell;
    my $delete_query = "DELETE FROM tell WHERE UPPER(tonick)=UPPER(?)";
    my $delete_handle = $dbh->prepare($delete_query);
    $delete_handle->execute($user);
  }
  $dbh->disconnect;
}

$array[0] = $login;
$array[1] = $pm;
$array[2] = $private;
$array[3] = $public;

## Do not alter following line
output(@array);
