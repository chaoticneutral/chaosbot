#!/usr/bin/perl
#+----------------------------------------------------------------------------+
#| Uploads Chat Room - for postage of Maglinks on ODCH                        |
#+-----------------------------------------------+----------------------------+
#| Author:  Derived from BigG's ChatBot by Anubis|
#| Created: 26/03/2006                           |
#+-----------------------------------------------+

use warnings;

# The name of the bot as it appears in the userlist
$botName = "OpChat";
# The version that the bot is
$version = 0.25;
# The path to the script - note: no trailing '/'
$scriptPath = "/home/hub/.opendchub/scripts";

# Command placeholder - what identifies a command
$cp = '-';
$c  = "";

# True and false
$true = 1;
$false = 0;

# The users that are currently connected to the chat
%users = ();

# If someone has been offered an invite to the chat they will be stored here
# until they have connected
%invited = ();

# The topic for the channel
@topic = ();

# the last 100 lines
@history = ();
$historyLine = 0;
$historyLength = 100;

#@winrars = 
#("Sjcs","Tachikoma","fastdude7","watzisneim","SemiPureAngel","Jay","Loki","Aenimal","Ace","Industrious_Thoughts","dloadergirl","Anubis","Smile","Murfle","Presario","Stoned_Turtle");

sub main() {
	&odch::data_to_all("<$botName> $botName version $version installed; configs loaded.|");
	&odch::register_script_name($botName);
	&load_data();
#-- Escape the command placeholder if relevant
	$c = escape_string($cp);
#	odch::data_to_all("$c|");
#	&odch::data_to_all("<$botName> $botName version $version loaded|");
	 # intialise the history
	for ($i = 0; $i < $historyLength; $i++){
	      $history[$i] = "no history";

	}
}

# Triggers when any data passes through the hub
sub data_arrival() {
	my($user, $data) = @_;

#-- Check that the data is a pm to the bot
	if($data =~ /^\$To:\s\Q$botName\E\sFrom:\s\Q$user\E\s\$\<\Q$user\E\>\s(.*)\|/s) {
		$chatString = $1;
		
#------ Only command that can be used whilst not connected
		if($chatString =~ /^(?:$c)join$/i) {
			if(&is_connected($user)) {
				&send_data($botName, "<$botName> You've already joined you derp! >_>");
			}
			else {
				&add_user_connection($user);
			}
		}
#------ Make sure that the user is connected
		if(&is_connected($user)) {
#---------- Send the line that the user has said to everybody else...
			&send_data( $user, "<$user> $chatString" );
			$history[$historyLine] = "<$user> $chatString";
			$historyLine = $historyLine + 1;
			if ($historyLine == $historyLength) {
			      $historyLine = 0;
			}
#---------- Sends a list of the commands available to the user - OPs have a greater
#---------- command set to invited users 
			if($chatString =~ /^(?:$c)commands$/i) {
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Commands:\n" .
$cp . "quit   - Exits the chat. To join again you will have to type .join\n" .
$cp . "leave  - Alias of $cp"."quit\n" .
$cp . "names  - Sends a list of all of the people who are currently connected to\n " .
      "          $botName.\n" .
$cp . "rules  - Gets the DT rules" );
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Operator only commands:\n" .
$cp . "invite <foo> - Invites the specified user to join the chat.  $botName will PM\n" .
      "                the user with an invite which they will have to respond with\n" .
	  "                the $cp"."join command\n" .
$cp . "kill <foo>   - Forcefully removes the specified user from the chat.  Can\n" .
      "                only be invoked on non-operators.\n" .
$cp . "ban <foo>    - Invokes ban.sh.  If foo is a nick, it will grab the IP from the hub\n" .
      "                [so do not use with stunnel users!!] otherwise, you may specify an\n" .
	  "                IPv4 Address xxx.xxx.xxx.xxx\n" .
$cp . "ip(s) <foo>  - Gets the ip address of the specified user/all users"
);
			}
			elsif($chatString =~ /^(?:$c)history\s*(.*)/ || $chatString =~ /^(?:$c)h\s*(.*)/) {
				
				$hlength = 10;
				@chatStrA = split(/ /, $chatString);
				
				if( $chatStrA[1]) {
					if ($chatStrA[1] =~ /^\d+$/){
						$hlength = $chatStrA[1];
					}
				}
				
				if ($hlength > $historyLength) {
					$hlength = $historyLength;
				}
				$temp = "";
				for ($i = 0; $i < $hlength; $i++) {
				    $temp .= "\n $history[($historyLine - $hlength + $i)%$historyLength]";
				}
				#$temp = "derp";

				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
						"[History]$temp|");
				
				# odch::data_to_all("$temp | ");
			}
			elsif($chatString =~ /^(?:$c)(?:leave|quit)$/i) {
				&remove_user_connection($user);
			}
			elsif($chatString =~ /^(?:$c)(?:niggers)$/i) {
                                &send_data($botName, "<NIGGER> I STEELZ HIS INTINET FO SHO");
				&remove_user_connection($user);
				
                        }

#---------- Lists the users that are currently connected to the bot - sorted
#---------- alphabetically, case insensitive
			elsif($chatString =~ /^(?:$c)names$/i) {
				$temp = "";
				foreach $nick (sort {lc $a cmp lc $b} keys %users) {
					$temp .= "\n[$nick]" if &odch::get_type($nick) != 0;
				}
				&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
						"[Users: $botName]$temp|");
			}
			
			elsif($chatString =~ /^(?:$c)topic(\s+(.*)|)$/i) {
				if($1 eq "") {
					&send_topic($user);
				}
				else {
					&set_topic($user, $2);
					&send_data( $botName, "<$botName> $user has changed the topic to: $2");
				}
			}
			elsif( $chatString =~ /^(?:$c)rules$/i ) {
				my $tmp = "";

				open( RULES, $scriptPath . "/rules" ) or &odch::data_to_all( "<$botName> Cannot open rules!|" );

				while( <RULES> ) {
					chomp;
					$tmp .= "$.. $_\n";
				}

				close RULES;

				&send_data( $botName, "<$botName> The rules have been set along the ages to be as follows:\n\n$tmp" );
			}
#---------- Operator only commands
			elsif(&is_op($user)) {
#-------------- Forcefully remove a non-OP from the bot - i.e. someone who has been
#-------------- invited for some reason.  The user has to be connected to the chatbot
#-------------- for the function to be invoked
				if( $chatString =~ /^(?:$c)kill\s+(.*)/ && !&is_op( $1 ) && &is_connected( $1 )) {
					&remove_user_connection( $1 );
					&send_data( $botName, "<$botName> $1 has been removed from the chat" );
					&odch::data_to_user( $1, "\$To: $1 From: $botName \$<$botName> " .
							"You have been disconnected from $botName by $user|" );
				}
				elsif($chatString =~ /^(?:$c)killall/) {
					foreach $nick ( keys %users ) {
						if(&odch::check_if_registered($nick) < 2) {
							&remove_user_connection($nick);
							&send_data($botName, "<$botName> $nick has been removed from the chat");
							&odch::data_to_user($nick, "\$To: $nick From: $botName \$<$botName> " .
									"You have been disconnected from $botName by $user|");
						}
					}
				}
#-------------- Invite a user to the chat
				elsif($chatString =~ /^(?:$c)invite\s+(.*)/) {
					&invite_user( $1 ) if &odch::get_type( $1 ) == 0;
				}
				elsif($chatString =~ /^(?:$c)ban\s(list|(add|delete|remove)\s((?:\d{1,3}\.){3}\d{1,3}|\w+))$/) {
					if( $1 eq "list" ) {
						&send_data( $botName, "<$botName> List of bans:\n" . `~/./ban.sh list` );
					}
					else {
						my $action = $2;
						my $who = $3;
						my $ip = "";
	
						if( $who =~ /\w+/ ) {
							$ip = &odch::get_ip( $who );
						}
						else {
							$ip = $who;
						}

						my @args = ( "/home/hub/ban.sh", $action, $ip );
						my $returnState = system( @args );

						if( $returnState == 0 ) {
							&send_data($botName, "<$botName> Ban $action for $who successful!");
						}
						else {
							&send_data($botName, "<$botName> Ban $action for $who unsuccessful!");
						}
					}
				}
				elsif($chatString =~ /^(?:$c)ip(s|\s+(.*))$/i) {
					if($1 eq "s") {
						my(@nicks) = split(/\s/, odch::get_user_list());
						my $temp = "";
						my %temp = ();

#---------------------- Generate a list of nicks who are connected to each IP.
						foreach $nick (@nicks) {
							$temp{&odch::get_ip( $nick )} .= "$nick ";
						}

						foreach $ip (keys %temp) {
							my $show = ( $ip eq "" ? "Bots" : $ip );
							$temp .= "\n\n$show:\n$temp{$ip}";
						}

						&send_data($botName, "<$botName> List of all users and IPs: $temp");
					}
					else {
						$nick = $2;
						my $ip = &odch::get_ip( $nick );

						if($ip ne "") {
							&send_data($botName, "<$botName> $2 has the IP: $ip");
						}
						else {
							&send_data($botName, "<$botName> $2 is not online!");
						}
					}
				}
			}
		}
#------ User is not connected to the chat
		else {
			&odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
					"You are not connected to the chat, please type $cp"."join.|");
		}
	}
}

# Triggers that occur when a user connects to the hub
sub op_admin_connected() {
	my($user) = @_;
	connect_message($user) if is_connected($user);
}
sub op_connected() {
	my($user) = @_;
	connect_message($user) if is_connected($user);
}
sub reg_user_connected() {
	my($user) = @_;
	connect_message($user) if is_connected($user);
}
sub new_user_connected() {
	my($user) = @_;
	connect_message($user) if is_connected($user);
}


# Sends the data to everyone connected to the chatbot, if they are online
sub send_data() {
	my($user, $data) = @_;

	foreach $nick (keys %users) {
		if($user ne $nick && odch::get_type($nick) != 0) {
			odch::data_to_user($nick, "\$To: $nick From: $botName \$$data|");
		}
	}
}

sub spam_main_chat() {
	my($data) = @_;
	my(@users) = split(/\s/, odch::get_user_list());
#-- Spam all users who are connected to ChatBOT informing them of the join	
	foreach $user (@users) {
		if($users{$user} ne "") {
			#odch::data_to_user($user, "<$botName> $data has joined $botName|");
		}
	}
}

sub escape_string() {
	my($string) = @_;
	$string =~ s/$_/$_/ for qw(\\\ \| \\( \\) \[ \{ \$ \+ \? \. \* \/ \^);
	return $string;
}



sub add_user_connection() {
	my($user) = @_;
#-- User is OP - they are automatically granted access
	if(is_op($user)) {
		$users{$user} = $true;
		&connect_message($user);
	}
#-- User has been sent an invite by someone in the channel already
	elsif(is_invited($user)) {
		$users{$user} = $true;
		delete $invited{$user};
		&connect_message($user);
	}
#-- Some random pleb tries it
	else {
		odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> You " .
				"are not allowed to join the chat!|");
	}
	&save_data();
	#&connect_message($user);
}

sub remove_user_connection() {
	my($user) = @_;
	delete $users{$user};
	&save_data();
#	&end_data($user, "<$botName> <-- Parts: $user");
}

sub connect_message() {
	my($user) = @_;
	
#	&send_data($user, "<$botName> --> Joins: $user");
	&spam_main_chat($user);

	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> Welcome to $botName! ".
			"Type $cp"."commands for available commands|");
	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> --- Topic for $botName is: $topic[3]|");
	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> --- Topic set by $topic[0] [$topic[1]] " .
			gmtime($topic[2]) . "|");
	$temp = "";
        for ($i = 0; $i < 10; $i++) {
        	$temp .= "\n $history[($historyLine - 10 + $i)%$historyLength]";
        }
                                #$temp = "derp";

        &odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> " .
        "[History]$temp|");
}

sub invite_user() {
	my($user) = @_;
	odch::data_to_user($user, "\$To: $user From: $botName \$<$botName> You " .
			"have been invited to join $botName chat.  Type $cp"."join to connect|");
	$invited{$user} = $true;
}


# Topic shiz
sub send_topic() {
	my(@user) = @_;
	&send_data($user, "<$botName> --- Topic for $botName is: $topic[3]");
	&send_data($user, "<$botName> --- Topic set by $topic[0] [$topic[1]] " . gmtime($topic[2]) ."");
}

sub set_topic() {
	my($user, $data) = @_;
	my($hostname) = odch::get_hostname($user);
	my($time) = time;
	@topic = ($user, $hostname, $time, $data);
	&send_data($botName, "<$botName> --- Topic set to \"[$topic[3]]\" by $topic[0]");
	&save_data();
}

# Checks
# Returns true if the user is an operator
sub is_op() {
	my($user) = @_;
	if(odch::get_type($user) >= 16) {
		return $true;
	}
	else {
		return $false;
	}
}

# Returns true if the user has been invited to join the chat
sub is_invited() {
	my($user) = @_;
	if($invited{$user} == $true) {
		return $true;
	}
	else {
		return $false;
	}
}

# Returns true if the user is connected to the chatbot
sub is_connected() {
	my($user) = @_;
	if($users{$user} eq $true) {
		return $true;
	}
	else {
		return $false;
	}
}


# File loading - config, who is connected etc
sub load_data() {
	open(DATA, "$scriptPath/$botName"."data"); # or odch::data_to_all("<$botName> Unable to load $botName data!|");
	
	while(<DATA>) {
		if($_ =~ /^\[topic\]\s(.*)\s(.*)\s(\d+)\s(.*)\n/) {
			@topic = ($1, $2, $3, $4);
		}
		elsif($_ =~ /^\[users\]\s(.*)\n/) {
			foreach $user (split(/\s/, $1)) {
				$users{$user} = 1 if $user ne "";
			}
		}
	}
	close(DATA);
}

# Save the data to file
sub save_data() {
	my($data) = "";
	
#-- Add topic to data string
	$data .= "[topic] @topic\n[users] ";

#-- Add the current users
	foreach $user (keys %users) {
		$data .= " $user";
	}
	
	open(DATA, ">$scriptPath/$botName"."data") or odch::data_to_all("<$botName> Unable to load $botName data!|");
	print DATA "$data\n";
	close(DATA);
}

#------------------------------------------------------------------------------

# EOF
