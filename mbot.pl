
#----------------------------------------------------+
#		MovieBot by Anubis		     +
#	For finding random movies on IMDb	     +
#						     +
#		<3<3<3<3<3<3<3			     +
#----------------------------------------------------+


$botname = "MovieBot";
$cp = '-';
$c = "";
$version = "1.3.9";

use POSIX qw/strftime/;
use LWP::Simple;
use HTML::Parser;
use Data::Dumper;
use HTML::Entities;
@desription = ();
%movieclicks = ();
$maxclicks = 3;
$date = strftime( "%d", localtime(time()) );
$status = 1;

sub main()
{
	odch::register_script_name($botname);
        &odch::data_to_all("<$botname> $botname version $version ready to give you delicious movies! The date identification number is $date.|");
        $c = escape_string($cp);
}

sub data_arrival(){

	my($user, $data) = @_;
	if( $data =~ /^\<\Q$user\E\>\s(.*)\|/ ) {
		my $chat = $1;
		if ( $date !~ (strftime( "%d", localtime(time()) ))){
			%movieclicks = ();
			$date = strftime( "%d", localtime(time()) );
		}
		if ( $chat =~ /^-game$/ ){
			odch::data_to_all("<$botname> $user You should play Solar 2! http://store.steampowered.com/app/97000/|");
}
			
                if ( $chat =~ /^-rmquota\s(\S*)$/ && ($user =~/KY/ || $user =~ /KYlite/ ) ) {
                        $clearuser = substr($chat, 9);
			if( $clearuser =~ /allusers/ ){
		        	odch::data_to_all("<$botname> Clearing quota for all users|");
                        	%movieclicks = "0";
			}else{
			odch::data_to_all("<$botname> Clearing quota for $clearuser|");
			$movieclicks{$clearuser} = "0";
			}
                }
		if ( $chat =~ /^(?:$c)gogogo$/ && ($user =~/KY/ || $user =~ /KYlite/ ) ) {
			$status = 1;
			odch::data_to_all("<$botname> I'm awake, ready to receive movie commands|");
		}
		if ( $chat =~ /^(?:$c)stfu$/ && ($user =~/KY/ || $user =~ /KYlite/ )) {
			$status = 0;
			odch::data_to_all("<$botname> I agree with you $user, I'll shut up|");
		}
		if (( $chat =~ /^(?:$c)checkquota$/ || $chat =~ /^(?:$c)cq$/) && ($status == 1 || $user =~/KY/ || $user =~ /KYlite/ )) {
			if ( exists($movieclicks{$user}) ){
				if ($movieclicks{$user} >= $maxclicks){
					odch::data_to_all("<$botname> $user, You have used all of your quota of $maxclicks in this 24 hour rotation.|");
				}else{
				odch::data_to_all("<$botname> $user, you have used $movieclicks{$user} out of your quota of $maxclicks in this 24 hour rotation.|");
				}
			}else{
				odch::data_to_all("<$botname> $user, you have not used any of your quota of $maxclicks in this 24 hour rotation.|");
			}
		}
		if ( $chat =~ /^(?:$c)movie$/ && ($status == 1 || $user =~/KY/ || $user =~ /KYlite/  )) {
			$movieclicks{$user} = $movieclicks{$user} + 1;
			if ( $movieclicks{$user} > $maxclicks &&! ($user =~/KY/ || $user =~ /KYlite/ ) ) {
				odch::data_to_all("<$botname> $user, You have exceeded your $botname quota for today, please try again tomorrow.|");
				odch::kick_user($user);
			#	$movieclicks{$user} = 0;
			}else{
			odch::data_to_user($user, "<$botname> Finding a random movie now...|");
			process($user);
			}
		}
                if ( $chat =~ /^(?:$c)simmovie/ && ($status == 1 || $user =~/KY/ || $user =~ /KYlite/  )) {
                       $movieclicks{$user} = $movieclicks{$user} + 1;
                       if ( $movieclicks{$user} > $maxclicks &&! ($user =~/KY/ || $user =~ /KYlite/ ) ) {
                               odch::data_to_all("<$botname> $user, You have exceeded your $botname quota for today, please try again tomorrow.|");
                                odch::kick_user($user);
                        #       $movieclicks{$user} = 0;
                        }else{
			$moviestring = substr($chat, 10);
			@simmoviearray = split(/ /, $moviestring);
			$joinedmoviestring = join("+", @simmoviearray);
                        odch::data_to_user($user, "<$botname> Finding similar movies now...|");
                        moprocess($user, $joinedmoviestring, $moviestring);
                        }
                }
               if ( $chat =~ /^(?:$c)simmusic/ && ($status == 1 || $user =~/KY/ || $user =~ /KYlite/  )) {
                       $movieclicks{$user} = $movieclicks{$user} + 1;
                       if ( $movieclicks{$user} > $maxclicks &&! ($user =~/KY/ || $user =~ /KYlite/ ) ) {
                               odch::data_to_all("<$botname> $user, You have exceeded your $botname quota for t
oday, please try again tomorrow.|");
                                odch::kick_user($user);
                        #       $movieclicks{$user} = 0;
                        }else{
                        $musicstring = substr($chat, 10);
                        @simmusicarray = split(/ /, $musicstring);
                        $joinedmusicstring = join("+", @simmusicarray);
                        odch::data_to_user($user, "<$botname> Finding similar artists now...|");
                        muprocess($user, $joinedmusicstring, $musicstring);
                        }
                }

	}
}

sub moprocess(){
	my ($mouser, $n1, $n2) = @_;
	my $moviecount = 0;
	my $mourl = "http://movie-map.com/$n1.html";	
	my $mopage = get($mourl);
	$outputsimmovie = "Similar Movies to $n2:\n";
	foreach ( split ("\n", $mopage)){
		if ($_ =~ /class=S id=s\d*>(.*)</){
			$moviecount++;
			if ( $moviecount >= 2 && $moviecount <= 11 ){
				$outputsimmovie .=  "$1\n";
			}
		}
	}
	odch::data_to_user($mouser, "<$botname> $outputsimmovie|");
}
sub muprocess(){
        my ($muuser, $m1, $m2) = @_;
        my $musiccount = 0;
        my $muurl = "http://music-map.com/$m1.html";
        my $mupage = get($muurl);
        $outputsimmusic = "Similar Artists to $m2:\n";
        foreach ( split ("\n", $mupage)){
                if ($_ =~ /class=S id=s\d*>(.*)</){
                        $musiccount++;
                        if ( $musiccount >= 2 && $musiccount <= 11 ){
                                $outputsimmusic .=  "$1\n";
                        }
                }
        }
        odch::data_to_user($muuser, "<$botname> $outputsimmusic|");
}



sub process(){
	my($muser) = @_;
	my $url = "http://imdb.com/random/title";  #parse the whole web page into a variable (will not go into array)
	my $ua = new LWP::UserAgent;
	$ua->timeout(120);
	my $url='http://imdb.com/random/title';
	my $request = new HTTP::Request('GET', $url);
	my $response = $ua->request($request);
	my $content = $response->content();
	$allowed = "1";
	foreach ( split ("\n", $content)) { #split into each new line and iterate over the lines until you can find the title and the canonical link
        	$eachline = $_;
	       	get_description($eachline);
        	if ( $_ =~ /\<title\>(.*)\s+-\s+IMDb\<\/title/ ){
                	$title = $1;
        	}
        	if ( $_ =~ /canonical/ ){
                	@linkline = split (/\s+/);
                	$link = substr($linkline[3], 6, -1);
        	}
		if ( $_ =~ /\(TV Series/ || $_ =~ /Not yet released/ ){
			$allowed = "0";
		#	odch::data_to_all("<$botname> $muser This is TV/New and content and request $request and $_|");
		}
	}

	if ( $allowed == "1" ){
		$title = decode_entities($title);
		$outputdescription = decode_entities($outputdescription);
		odch::data_to_user($muser, "<$botname> $muser You should watch $title - $link|");
		odch::data_to_user($muser, "<$botname> Description of $title:\n$outputdescription\n\n$link|");
		@description = ();
	}else{
		@description = ();
		#odch::data_to_all("<$botname> derpoderpderp|");
		process();
	}
}

sub get_description(){
        my($line) = @_;
        if ( $line =~ /name=\"description\".*content=\"(.*)\" \/\>/ ){
                $outputdescription = $1;
                if (length($outputdescription) < 5 ){
                        $outputdescription = "No Description available, please go to IMDb for a storyline.";
                }
        }
}


sub escape_string(){
        my($string) = @_;
        $string =~ s/$_/$_/ for qw(\\\ \| \\( \\) \[ \{ \$ \+ \? \. \* \/ \^);
                return $string;
}

sub is_op() {
        my($user) = @_;
        if(odch::get_type($user) >= 16) {
		return 1;
        }else{
                return 0;
        }
}
