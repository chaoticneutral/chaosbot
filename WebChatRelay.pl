#----------------------------------------------------------------------------------+ 
#         Relay  - Hub Bot for Chaotic Neutral Hub - ANU Campus DC HUB             |
#                    WebRelayChat - for web=>hub interaction			   |
#                            Written by Anubis               			   |
#----------------------------------------------------------------------------------+

## Variables section: need POSIX for the timestamp on the logs and need
##Time::Hi:Res for the wait command (although I've changed that from sleep
## to using a hacky one called 'select' so i can get under one second
##can always go back to using sleep instead
##VERY IMPORTANNT: MUST HAVE $SIG{CHLD} SET TO IGNORE
##if it isn't then the bot will keep making forks and trying to keep up with
##the zombie processes. By not reaping the children it ignores them and they don't 
##become zombie
use POSIX qw/strftime/;

use Config::IniFiles;                   #For the conf file
use Switch;                             #For Switch/case statements
use Text::Tabs;                         #For expand in sendmessage
use IPC::System::Simple qw(capture);    #For the capture() command when calling external scripts
use DBI;                                #for connecting to the logging database;
use DateTime;                           #for timezone support when logging chat

my $ConfigFile = "/home/hub/.opendchub/scripts/chaosbot.conf";
tie my %ini, 'Config::IniFiles', (-file => $ConfigFile);
my %Config = %{$ini{"config"}};
#------------Add in the sleep so the db can be created before we query it.
sleep(2);

my $dbh = DBI->connect("dbi:SQLite:" . $Config{dbloc} . $Config{db},"","");
$logname = $Config{log};

$scriptPath = "/home/hub/.opendchub/scripts";
$botname = "WebRelayBot";
$webstatus = "1";
$data = "0";
use Time::HiRes qw(usleep nanosleep);
$scriptPath = "/home/hub/.opendchub/scripts";
$SIG{CHLD} = 'IGNORE';
sub main()
{
   # 	odch::register_script_name($botname);
	&odch::data_to_all("<$botname> $botname Status $webstatus!|");
	&child();
}
sub data_arrival()
{
        my($user, $data) = @_;
	if( $data =~ /^\<\Q$user\E\>\s(.*)\|/ ){
                my $chat = $1;
		if (is_op($user) && $chat =~ /^-pid/ ){
			 odch::data_to_all("<$botname> The PID for WebChat is $pid|");	
		}
		if (is_op($user) && $chat =~ /^-killweb/ ){
			odch::data_to_all("<$botname> WebChat Process Killed!|");
			kill 1, $pid;
			$webstatus = 0;
		}
		if (is_op($user) && $chat =~ /^-startweb/  && $webstatus == 0) {
			odch::data_to_all("<$botname> Starting Web|");
			&child();
		}
		if (is_op($user) && $chat =~ /^-webstatus/) {
			if ($webstatus) {
				 odch::data_to_all("<$botname> Web client is Online|");
			} else {
				 odch::data_to_all("<$botname> Web client is Offline|");
			}
		}
		if (is_op($user) && $chat =~ /^-emptychat/) {
			&empty();
		}
	}	
} 
########################################################################################
sub is_op() {
	my($user) = @_;
	if(odch::get_type($user) >= 16) {
		return 1;
	}
	else {
		return 0;
	}
}

#sub log_data() {
#        my($chatString) = @_;
#        $time = strftime( "%Y\-%m\-%d %H\:%M\:%S", localtime(time()) );
#        $logyear = strftime( "%Y", localtime(time()) );
#        open (MDA, ">>$scriptPath/logs/CNLog-overall.txt" );
#	odch::data_to_all("<$botname> !|");
#        open (MDB, ">>$scriptPath/logs/CNLog-$logyear.txt" );
#        open (MDC, ">>/var/www/log"); #for the live page
#        print MDA "\[$time\] ", "$chatString \n";
#        print MDB "\[$time\] ", "$chatString \n";
#        print MDC "\[$time\] ", "$chatString \n";
#        close(MDA);
#        close(MDB);
#        close(MDC);
#}

sub log_data() {
  my ($chatstring) = @_;
  my ($unfuser, $chat) = split('> ', $chatstring);
  my ($nothing, $user) = split('<', $unfuser);
  my $query = "INSERT INTO $logname (unixtime, timestamp, nick, chat) VALUES (?, ?, ?, ?)";
  my $handle = $dbh->prepare($query);
  $handle->bind_param( 1, time());
  $handle->bind_param( 2, timestamp());
  $handle->bind_param( 3, $user);
  $handle->bind_param( 4, $chat);
  $handle->execute;
  # For the flag file for live
  open( FLAG, '>' . $Config{scriptPath} . 'flagfile');
  print FLAG $chatstring;
  close (FLAG);
}

sub timestamp() {
  my $now_string = DateTime->now();
  $now_string->set_time_zone($Config{timezone});
  return $now_string->strftime("%Y-%m-%d %H:%M:%S");
}


sub child() {
 $webstatus = 1;
 $pid=fork(); ##Need this here or odch thinks the script is dead (because of the sleep and makes another one - big problems without
        if($pid == 0) {
                while(1){ ##consider using a trigger such as Inotify2 instead of while
                        $LOGFILE = "/home/hub/.opendchub/scripts/webchat";
                        open(LOGFILE) or die("Could not open log file.");
                        foreach $line (<LOGFILE>) { ##in case the hub dies and there is an accumulation of lines written from the web they'll all get pasted into MC when the hub lives again
                                chomp($line);
                                odch::data_to_all("$line|");
                                &log_data($line);
                        }
                close LOGFILE;
                &empty();
                select(undef, undef, undef, 0.01);
                }
        }
	pid_log();
}

sub empty() {
 open DONE, ">/home/hub/.opendchub/scripts/webchat";
                print DONE "";
                close DONE;
}

sub pid_log() {
	$time = localtime(time);
	open PIDF, ">>/home/hub/.opendchub/scripts/webprocess";
	print PIDF "PID: \[$pid\] ", "Time: $time \n";
	close (PIDF);
}
